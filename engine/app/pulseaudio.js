const execAsync = require('async-child-process').execAsync;
const logger = require('./logger');

const start = exports.start = async function () {
    try {
        await execAsync('pulseaudio -D');
    } catch (error) {
        logger.log("Pulse audio failed to start: " + error)
    }
};

const createSink = exports.createSink = async function (sinkName) {
    let sinkId = await readSinkId(sinkName);

    if (sinkId) {
        logger.log("Existing Sink id: " + sinkId)
        return sinkId;
    }
    await execAsync('pactl load-module module-null-sink sink_name=' +
        sinkName + ' sink_properties=device.description=' + sinkName);

    sinkId = await readSinkId(sinkName);
    logger.log("New Sink id: " + sinkId);
    return sinkId;
};

const setDefaultSink = exports.setDefaultSink = async function () {
    logger.log("Setting default sink to 'Default'");
    const defaultSink = "Default";
    const defaultSource = defaultSink + ".monitor";
    await createSink(defaultSink);
    await execAsync('pacmd set-default-sink ' + defaultSink);
    const {stdout} = await execAsync('pacmd set-default-source ' + defaultSource);
    return stdout.trim();
};

const readSinkId = exports.readSinkId = async function (sinkName) {
    const {stdout} = await execAsync('pactl list short sinks | grep ' + sinkName + '| cut -f1');
    return stdout.trim();
};

const getInputId = exports.getInputId = async function (chromePid) {
    const {stdout} = await execAsync('./scripts/get_input_index.sh ' + chromePid);
    const inputIdList = stdout.trim().split(" ");
    logger.log("Input id: " + inputIdList);
    return inputIdList;
};

const moveInput = exports.moveInput = async function (inputId, sinkId) {
    logger.log("Moving Input id: " + inputId + " to Sink id: " + sinkId);
    const {stdout} = await execAsync('pacmd move-sink-input ' + inputId + ' ' + sinkId);
    return stdout.trim();
};
